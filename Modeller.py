import pickle
from PIL import Image
import numpy as np
import csystem
import math
import os
class Modeller:
    def __init__(self, r, h, smoothstep):
        self.r = r
        self.h = h
        self.smoothstep = smoothstep
        self.faces = []
        self.points_indices = {}
        self.points = []
        self.neighbours = {}


    def construct(self, input, output_format):
        self.faces = []
        self.points_indices = {}
        self.points = []
        self.neighbours = {}

        Nxy = input.shape[0]-1
        Nz  = input.shape[2]-1

        #En chaque point, selon si absence de voisin on va pouvoir dessiner une face (plusieurs sous-faces en fait car on dessiner avec des sous-faces triangules soit un hexagone (base du prisme fondamental) soit un rectangle (hauteur du prisme fondamental))
        # En définissant chaque triangle par ses points, on fait attention de les mettre dans le sens direct (anti-horaire)
        # Un hexagone sera défini par 7 points : son centre, le point h(haut), hg(haut gauche), hd(haut droit), bg(bas gauche), bd(bas droit (mais je préfère la quézac)), et b(bas)
        #Il faut faire attention si ces points existent car on travaille sur 1/24e d'espace ce qui coupe certaines hexagones.
        #  /\
        # /  \
        #|    |
        #|    |
        #\   /
        # \/

        # Un rectangle par r00b (bas "gauche"), r00h (haut "gauche"), r11b(bas "droit"), r11h(haut "droit")
        for k in range(Nz + 1):
            for j in range(Nxy + 1):
                for i in range(j, Nxy + 1):
                    if(input[i,j,k] == 1.):
                        if(k == Nz or input[i,j,k+1] != 1.):#pas de voisin au dessus -> hexagone
                            c  = (i, j, k+1, "c")
                            hd = (i, j, k+1, "hd")
                            bd = (i, j, k+1, "bd")
                            if(i != j):
                                h  = (i, j, k+1, "h")
                                hg = (i, j, k+1, "hg")
                            if(j != 0):
                                b  = (i, j, k+1, "b")
                            if(j != 0 or i != 0):
                                bg = (i, j, k+1, "bg")

                            if(i == 0 and j == 0):
                                self.addFace(c,bd,hd, Nxy)
                            elif(i == j):
                                self.addFace(c,bd,hd, Nxy)
                                self.addFace(c,bg,b, Nxy)
                                self.addFace(c,b,bd, Nxy)
                            elif(j == 0):
                                self.addFace(c,bd,hd, Nxy)
                                self.addFace(c,h,hg, Nxy)
                                self.addFace(c,hg,bg, Nxy)
                                self.addFace(c,hd,h, Nxy)
                            else:
                                self.addFace(c,bd,hd, Nxy)
                                self.addFace(c,h,hg, Nxy)
                                self.addFace(c,hg,bg, Nxy)
                                self.addFace(c,bg,b, Nxy)
                                self.addFace(c,b,bd, Nxy)
                                self.addFace(c,hd,h, Nxy)

                        if(k == 0 or input[i,j,k-1] != 1.):#pas de voisin en dessous -> hexagone
                            c  = (i, j, k, "c")
                            hd = (i, j, k, "hd")
                            bd = (i, j, k, "bd")
                            if(i != j):
                                h  = (i, j, k, "h")
                                hg = (i, j, k, "hg")
                            if(j != 0):
                                b  = (i, j, k, "b")
                            if(j != 0 or i != 0):
                                bg = (i, j, k, "bg")

                            if(i == 0 and j == 0):
                                self.addFace(c,bd,hd, Nxy)
                            elif(i == j):
                                self.addFace(c,bd,hd, Nxy)
                                self.addFace(c,bg,b, Nxy)
                                self.addFace(c,b,bd, Nxy)
                            elif(j == 0):
                                self.addFace(c,bd,hd, Nxy)
                                self.addFace(c,h,hg, Nxy)
                                self.addFace(c,hg,bg, Nxy)
                                self.addFace(c,hd,h, Nxy)
                            else:
                                self.addFace(c,bd,hd, Nxy)
                                self.addFace(c,h,hg, Nxy)
                                self.addFace(c,hg,bg, Nxy)
                                self.addFace(c,bg,b, Nxy)
                                self.addFace(c,b,bd, Nxy)
                                self.addFace(c,hd,h, Nxy)
                        if(i != j and input[i-1, j, k] != 1.): #pas de voisin à gauche -> rectangle
                            r00b = (i, j, k, "hg")
                            r00h = (i, j, k+1, "hg")
                            r11b = (i, j, k, "bg")
                            r11h = (i, j, k+1, "bg")

                            self.addFace(r00b,r11b,r11h, Nxy)
                            self.addFace(r00b,r11h,r00h, Nxy)
                        if((i != Nxy and input[i+1, j, k] != 1.) or i == Nxy): #pas de voisin à droite -> rectangle
                            r00b = (i, j, k, "bd")
                            r00h = (i, j, k+1, "bd")
                            r11b = (i, j, k, "hd")
                            r11h = (i, j, k+1, "hd")

                            self.addFace(r00b,r11b,r11h, Nxy)
                            self.addFace(r00b,r11h,r00h, Nxy)
                        if(j != 0 and input[i, j-1, k] != 1.): #pas de voisin en bas à gauche-> rectangle
                            r00b = (i, j, k, "bg")
                            r00h = (i, j, k+1, "bg")
                            r11b = (i, j, k, "b")
                            r11h = (i, j, k+1, "b")

                            self.addFace(r00b,r11b,r11h, Nxy)
                            self.addFace(r00b,r11h,r00h, Nxy)
                        if(j != Nxy and i > j and input[i, j+1, k] != 1.): #pas de voisin en haut à droite-> rectangle
                            r00b = (i, j, k, "hd")
                            r00h = (i, j, k+1, "hd")
                            r11b = (i, j, k, "h")
                            r11h = (i, j, k+1, "h")

                            self.addFace(r00b,r11b,r11h, Nxy)
                            self.addFace(r00b,r11h,r00h, Nxy)
                        if(i != j and input[i-1, j+1, k] != 1.): #pas de voisin en haut à gauche-> rectangle
                            r00b = (i, j, k, "h")
                            r00h = (i, j, k+1, "h")
                            r11b = (i, j, k, "hg")
                            r11h = (i, j, k+1, "hg")

                            self.addFace(r00b,r11b,r11h, Nxy)
                            self.addFace(r00b,r11h,r00h, Nxy)
                        if((i != Nxy and j != 0 and input[i+1, j-1, k] != 1.) or (i == Nxy and j != 0)): # pas de voisin en bas à droite-> rectangle
                            r00b = (i, j, k, "b")
                            r00h = (i, j, k+1, "b")
                            r11b = (i, j, k, "bd")
                            r11h = (i, j, k+1, "bd")

                            self.addFace(r00b,r11b,r11h, Nxy)
                            self.addFace(r00b,r11h,r00h, Nxy)

        #Smoothing time !
        #On moyenne les coordonnées de chaque point avec ses voisins, <smoothstep> fois
        for s in range(self.smoothstep):
            smoothed_points = [0]*len(self.points)
            for i in range(len(self.points)):
                x = self.points[i][0]
                y = self.points[i][1]
                z = self.points[i][2]

                for n in self.neighbours[i]:
                    p = self.points[n]
                    x += p[0]
                    y += p[1]
                    z += p[2]
                nbn = len(self.neighbours[i])
                x /= 1 + nbn
                y /= 1 + nbn
                z /= 1 + nbn
                smoothed_points[i] = (x,y,z)

            self.points = smoothed_points.copy()

        #saving time !
        if(output_format == ".obj"):
            obj = ""
            for p in self.points:
                obj += "v " + "%.3f"%(p[0]) + " " + "%.3f"%(p[1]) + " " + "%.3f"%(p[2]) + "\n"
                # obj += "v " + str(p[0]) + " " + str(p[1]) + " " + str(p[2]) + "\n"
            for f in self.faces:
                #indexing begins at 1 in obj files
                obj += "f " + str(f[0] + 1) + " " + str(f[1] + 1) + " " + str(f[2] + 1) + "\n"

            return obj


        elif(output_format == ".pov"):
            povray = "mesh2 {\nvertex_vectors {\n" + str(len(self.points))
            for p in self.points:
                povray += ",\n<" + "%.3f"%(p[0]) + "," + "%.3f"%(p[1]) + "," + "%.3f"%(p[2]) + ">"
            povray += "\n}\nface_indices{\n" + str(len(self.faces))
            for f in self.faces:
                povray += ",\n<" + str(f[0]) + "," + str(f[1]) + "," + str(f[2]) + ">"
            povray += """
}
"""

            return povray


        else:
            print("[ERROR] unknown output format " + str(self.output_format))


    #Identifie uniquement un point (coordonnées xyz + position sur l'hexagone xyz (c, h, hd, bd, b, bg, hg))
    #Compliqué mais pas tant avec un dessin mais je vais pas le faire :-)
    def getIndex(self, pos, Nxy):
        i, j, k, o = pos
        base = k*(10*Nxy + int(3*((Nxy - 1)*(Nxy - 2))/2))
        if(i == 0 and j == 0):
            c = base
            if(o == "c"):
                return c
            elif(o == "hd"):
                return c + 1
            elif(o == "bd"):
                return c + 2
            else:
                print("incorrect offset " + str(o) + " for (i,j) = (" + str(i) + "," + str(j) + ")")
        elif( i == 1 and j == 0):
            c = base + 3
            if(o == "c"):
                return c
            elif(o == "h"):
                return c + 1
            elif(o == "hd"):
                return c + 2
            elif(o == "bd"):
                return c + 3
            elif(o == "hg"):
                return self.getIndex((i - 1, j, k, "hd"), Nxy)
            elif(o == "bg"):
                return self.getIndex((i - 1, j, k, "bd"), Nxy)
            else:
                print("incorrect offset " + str(o) + " for (i,j) = (" + str(i) + "," + str(j) + ")")
        elif( i > 1 and j == 0):
            c = base + 3 + 4*(i-1)
            if(o == "c"):
                return c
            elif(o == "h"):
                return c + 1
            elif(o == "hd"):
                return c + 2
            elif(o == "bd"):
                return c + 3
            elif(o == "hg"):
                return self.getIndex((i - 1, j, k, "hd"), Nxy)
            elif(o == "bg"):
                return self.getIndex((i - 1, j, k, "bd"), Nxy)
            else:
                print("incorrect offset " + str(o) + " for (i,j) = (" + str(i) + "," + str(j) + ")")
        elif(i == j):
            c = base + 3 + 4*Nxy
            if(j >= 2):
                for l in range(2, j+1):
                    c += 6 + 3*Nxy - 3*l
            if(o == "c"):
                return c
            elif(o == "hd"):
                return c + 1
            elif(o == "bd"):
                if(i == Nxy):
                    return c + 2
                else:
                    return self.getIndex((i + 1, j - 1, k, "h"), Nxy)
            elif(o == "b"):
                return self.getIndex((i, j - 1, k, "hd"), Nxy)
            elif(o == "bg"):
                return self.getIndex((i, j - 1, k, "h"), Nxy)
            else:
                print("incorrect offset " + str(o) + " for (i,j) = (" + str(i) + "," + str(j) + ")")
        else:
            c = self.getIndex((j, j, k, "c"), Nxy) + 2 + 3*(i - j - 1)
            if(o == "c"):
                return c
            elif(o == "h"):
                return c + 1
            elif(o == "hd"):
                return c + 2
            elif(o == "bd"):
                if(i == Nxy):
                    return c + 3
                else:
                    return self.getIndex((i + 1, j - 1, k, "h"), Nxy)
            elif(o == "b"):
                return self.getIndex((i, j - 1, k, "hd"), Nxy)
            elif(o == "bg"):
                return self.getIndex((i, j - 1, k, "h"), Nxy)
            elif(o == "hg"):
                return self.getIndex((i - 1, j, k, "hd"), Nxy)
            else:
                print("incorrect offset " + str(o) + " for (i,j) = (" + str(i) + "," + str(j) + ")")


    # Ajoute une face à partir de trois points donnés
    # Sauf qu'en fait ça en rajoute 24 parce que symétries
    def addFace(self, p1, p2, p3, Nxy):
        #On duplique chaque point en 24 qu'on identifie par leurs indices
        indices1 = self.addPoint(p1, Nxy)
        indices2 = self.addPoint(p2, Nxy)
        indices3 = self.addPoint(p3, Nxy)

        cci = [0, 1, 2, 3, 4, 5, 18, 19, 20, 21, 22, 23]#counter clockwise indices : p, rot, -Zsym, -Zsymrot
        ci = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17] #clockwsie indices : sym, symrot, -Zp, -Zrot
        # On forme les faces triangulaires
        faces = set([(indices1[i], indices2[i], indices3[i]) for i in cci] + [(indices1[i], indices3[i], indices2[i]) for i in ci])

        for f in faces:
            self.faces.append(f)
            i1 = f[0]
            i2 = f[1]
            i3 = f[2]

            # On ajoute l'information des voisins de tel ou tel point
            if(i1 in self.neighbours):
                self.neighbours[i1].update([i2, i3])
            else:
                self.neighbours[i1] = set([i2, i3])
            if(i2 in self.neighbours):
                self.neighbours[i2].update([i1, i3])
            else:
                self.neighbours[i2] = set([i1, i3])
            if(i3 in self.neighbours):
                self.neighbours[i3].update([i1, i2])
            else:
                self.neighbours[i3] = set([i1, i2])


    #Ajoute un point en 24 fois en prenant en compte les symétries
    # pas si compliqué mais faut prendre en compte certains points spéciaux dont la transformation donne eux-même donc on ne créé pas de nouveau point
    def addPoint(self, p, Nxy):
        cos6 = math.cos(math.pi/6.)
        sin6 =  math.sin(math.pi/6.)
        cos3 = math.cos(math.pi/3.)
        sin3 =  math.sin(math.pi/3.)
        cos23 = math.cos(2.*math.pi/3.)
        sin23 =  math.sin(2.*math.pi/3.)

        i, j, k, o = p
        index = self.getIndex(p, Nxy)

        if(index not in self.points_indices):
            y = 3.*self.r/2.*j
            x = 2*self.r*cos6*i + self.r*cos6*j
            z = -self.h/2 + k*self.h
            if(z < 0):
                z = 0

            if(o == "h"):
                y += self.r
            elif(o == "hd"):
                x += self.r*cos6
                y += self.r*sin6
            elif(o == "bd"):
                x += self.r*cos6
                if(j != 0):
                    y -= self.r*sin6
            elif(o == "b"):
                y -= self.r
            elif(o == "bg"):
                x -= self.r*cos6
                if(j != 0):
                    y -= self.r*sin6
            elif(o == "hg"):
                x -= self.r*cos6
                y += self.r*sin6

            p = (x, y, z)
            sym = p
            rot1 = p
            symrot1 = p
            rot2 = p
            symrot2 = p
            rot3 = p
            symrot3 = p
            rot4 = p
            symrot4 = p
            rot5 = p
            symrot5 = p
            hasSym = not( (i == j and (o == "c" or o == "bg" or o == "hd")) or (i == j + 1 and (o == "hg" or o == "h")))
            hasRot = not(i == 0 and j == 0 and o == "c")


            ind = len(self.points)
            indices = []
            indices.append(ind)
            self.points.append(p)
            #5 rotations
            if(not(hasRot)):
                indices = indices*6
            else:
                #rot1
                ind += 1
                indices.append(ind)
                rot1 = (cos3*x - sin3*y, sin3*x + cos3*y, z)
                self.points.append(rot1)

                #rot2
                ind += 1
                indices.append(ind)
                rot2 = (cos23*x - sin23*y, sin23*x + cos23*y, z)
                self.points.append(rot2)

                #rot3
                ind += 1
                indices.append(ind)
                rot3 = (-x, -y, z)
                self.points.append(rot3)

                #rot4
                ind += 1
                indices.append(ind)
                rot4 = (cos23*x + sin23*y, - sin23*x + cos23*y, z)
                self.points.append(rot4)

                #rot5
                ind += 1
                indices.append(ind)
                rot5 = (cos3*x + sin3*y, - sin3*x + cos3*y, z)
                self.points.append(rot5)



            #sym i = j
            if(not(hasSym)):
                indices = indices*2
            else:
                if(hasRot):#just if not  point 00zc
                    if((j == 0 and (o == "c" or o == "bg" or o == "bd"))):
                        indices.extend([indices[1], indices[2], indices[3], indices[4], indices[5], indices[0]])
                    else:
                        xp = cos3*x + cos6*y
                        yp = sin3*x -sin6*y
                        #sym
                        ind += 1
                        indices.append(ind)
                        sym = (xp, yp, z)
                        self.points.append(sym)

                        #symrot1
                        ind += 1
                        indices.append(ind)
                        symrot1 = (cos3*xp - sin3*yp, sin3*xp + cos3*yp, z)
                        self.points.append(symrot1)

                        #symrot2
                        ind += 1
                        indices.append(ind)
                        symrot2 = (cos23*xp - sin23*yp, sin23*xp + cos23*yp, z)
                        self.points.append(symrot2)

                        #symrot3
                        ind += 1
                        indices.append(ind)
                        symrot3 = (-xp, -yp, z)
                        self.points.append(symrot3)

                        #symrot4
                        ind += 1
                        indices.append(ind)
                        symrot4 = (cos23*xp + sin23*yp, - sin23*xp + cos23*yp, z)
                        self.points.append(symrot4)

                        #symrot5
                        ind += 1
                        indices.append(ind)
                        symrot5 = (cos3*xp + sin3*yp, - sin3*xp + cos3*yp, z)
                        self.points.append(symrot5)

            #z sym
            if(k == 0):
                indices = indices*2
            else:
                if(not(hasRot)):
                    ind += 1
                    indices.extend([ind]*12)
                    self.points.append((p[0], p[1], -p[2]))
                elif(not(hasSym)):
                    ind += 1
                    indices.append(ind)
                    self.points.append((p[0], p[1], -p[2]))

                    ind += 1
                    indices.append(ind)
                    self.points.append((rot1[0], rot1[1], -rot1[2]))

                    ind += 1
                    indices.append(ind)
                    self.points.append((rot2[0], rot2[1], -rot2[2]))

                    ind += 1
                    indices.append(ind)
                    self.points.append((rot3[0], rot3[1], -rot3[2]))

                    ind += 1
                    indices.append(ind)
                    self.points.append((rot4[0], rot4[1], -rot4[2]))

                    ind += 1
                    indices.append(ind)
                    self.points.append((rot5[0], rot5[1], -rot5[2]))

                    ind -= 5
                    indices.extend([ind + l for l in range(6)])
                else:
                    if((j == 0 and (o == "c" or o == "bg" or o == "bd"))):
                        ind += 1
                        indices.append(ind)
                        self.points.append((p[0], p[1], -p[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot1[0], rot1[1], -rot1[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot2[0], rot2[1], -rot2[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot3[0], rot3[1], -rot3[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot4[0], rot4[1], -rot4[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot5[0], rot5[1], -rot5[2]))

                        indices.extend([indices[13], indices[14], indices[15], indices[16], indices[17], indices[12]])
                    else:
                        ind += 1
                        indices.append(ind)
                        self.points.append((p[0], p[1], -p[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot1[0], rot1[1], -rot1[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot2[0], rot2[1], -rot2[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot3[0], rot3[1], -rot3[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot4[0], rot4[1], -rot4[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((rot5[0], rot5[1], -rot5[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((sym[0], sym[1], -sym[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((symrot1[0], symrot1[1], -symrot1[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((symrot2[0], symrot2[1], -symrot2[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((symrot3[0], symrot3[1], -symrot3[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((symrot4[0], symrot4[1], -symrot4[2]))

                        ind += 1
                        indices.append(ind)
                        self.points.append((symrot5[0], symrot5[1], -symrot5[2]))


            self.points_indices[index] = indices

        return self.points_indices[index]
