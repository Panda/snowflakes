from PIL import Image
import math
import numpy as np
import csystem
import pickle

# Gère la croissance d'un flocon comme décrit dans le papier de référence
class System:
    #Initialise les paramètres importants + gèle le centre du flocon (graine)
    def __init__(self, Nxy, Nz, rho, beta, kappa, mu):
        self.Nxy = Nxy
        self.Nz = Nz

        self.rho = rho

        self.beta = beta
        self.kappa = kappa
        self.mu = mu

        self.a = np.zeros((self.Nxy+1, self.Nxy+1, self.Nz + 1))
        self.b = np.zeros((self.Nxy+1, self.Nxy+1, self.Nz + 1))
        self.d = self.rho*np.ones((self.Nxy+1, self.Nxy+1, self.Nz + 1))

        self.outer_bound = {}
        self.xyradius = 0
        self.zradius = 0

        self.seed()

    #Définis les parties initiales gelées
    def seed(self):
        for i in range(3):
            self.a[i][0][0]=1
            self.d[i][0][0]=0.
        self.a[1][1][0]=1
        self.d[1][1][0]=0.
        #Trouve la frontière du flocon
        self.find_outer_bound()

    #Définis la frontière "active" du flocon
    def find_outer_bound(self):
        for i in range(self.Nxy+1):
            for j in range(self.Nxy+1):
                for k in range(self.Nz + 1):
                    pos = (i,j,k)
                    if(self.a[pos] == 1.):
                        self.add_outer_bound(pos)

    #Quand on gèle un point qui devient flocon, met à jour la frontière "active" du flocon
    def add_outer_bound(self, pos):
        for neighbour in set(self.Tneighbours(pos)):
            if(self.a[neighbour] != 1.):
                if(neighbour not in self.outer_bound):
                    self.outer_bound[neighbour] = [1,0]
                    self.xyradius = max(self.xyradius,(max(neighbour[0], neighbour[1])))
                else:
                    self.outer_bound[neighbour][0] += 1
        for neighbour in set(self.Zneighbours(pos)):
            if(self.a[neighbour] != 1.):
                if(neighbour not in self.outer_bound):
                    self.outer_bound[neighbour] = [0,1]
                    self.zradius = max(self.zradius,(neighbour[2]))
                else:
                    self.outer_bound[neighbour][1] += 1


    #Croissance du flocon : diffusion de la vapeur, gel éventuel, fusion éventuelle
    def grow(self):
        #Appel à du code en cpython pour la diffusion car prend énormément de temps
        #voir csystem.pyx et setup.py
        csystem.diffusion(self.a, self.d, self.rho)
        self.freezing()
        self.melting()


    # gestion du gel
    def freezing(self):
        dp = self.d.copy()
        bp = self.b.copy()
        for pos, neighbours in self.outer_bound.copy().items():
            nt = neighbours[0]
            nz = neighbours[1]
            #Gel automatique si beaucoup de voisins gelés
            if(nt >= 4 or nz >= 2):
                self.freeze(pos)
            else:
                #Calcul des taux de gel
                config = str(nt)+str(nz)
                newb = self.b[pos] + (1 - self.kappa[config])*self.d[pos]
                bp[pos] =  newb

                newd = self.kappa[config]*self.d[pos]
                dp[pos] = newd
                #En cas du dépassement du seuil : gel total - fait partie du flocon
                if(newb >= self.beta[config]):
                    self.freeze(pos)


        self.b= bp.copy()
        self.d = dp.copy()

    # Admission dans flocon
    def freeze(self, pos):
        self.a[pos] = 1.0
        del self.outer_bound[pos]
        self.add_outer_bound(pos)

    # Fusion à la frontière
    def melting(self):
        dp = self.d.copy()
        bp = self.b.copy()
        for pos, neighbours in self.outer_bound.items():
            nt = min(neighbours[0],3)
            nz = min(neighbours[1],1)
            config = str(nt)+str(nz)

            #calcul des taux de fusion
            newb = (1 - self.mu[config])*self.b[pos]
            bp[pos] = newb

            newd = self.d[pos] + self.mu[config]*self.b[pos]
            dp[pos] = newd

        self.b = bp.copy()
        self.d = dp.copy()


    #On ne gère qu'1/24 du flocon et profitons des symétries inhérentes à celui-ci.
    #Si, en cherchant des voisins, ils sortent de ce sous-espace, on calcule son équivalent dans le sous-espace
    def sym24(self, pos):
        i,j,k = pos
        ip = i
        jp = j
        kp = k
        sum = i + j
        if(i < 0):
            if(j <= 0):
                ip = -i
                jp = -j
            elif(sum <= 0):
                ip = j
                jp = -sum
        if(i <= 0 and j > 0 and sum > 0):
            ip = sum
            jp = -i
        elif(i > 0 and j < 0 and sum >= 0):
                ip = -j
                jp = sum
        elif(i >= 0 and j < 0 and sum < 0):
            ip = -sum
            jp = i
        if(k < 0):
            kp = -k
        if(ip < jp):
            tmp = ip
            ip = jp
            jp = tmp
        return (ip,jp,kp)

    #Donne les points voisins du flocon sur le même plan (grille hexagonale)
    def Tneighbours(self, pos):
        i,j,k = pos
        dj = 1
        di = 1
        if(j == self.Nxy):
            dj = -2*self.Nxy
        if(i == self.Nxy):
            di = -2*self.Nxy
        return [self.sym24(pos), self.sym24((i-1,j,k)), self.sym24((i,j-1,k)),self.sym24((i-1, j+dj, k)), self.sym24((i, j+dj, k)), self.sym24((i+di, j, k)), self.sym24((i+di, j-1, k))]

    #Donne les points voisins au dessus et en dessous
    def Zneighbours(self, pos):
        i,j,k = pos
        dk = 1
        if(k == self.Nz):
            dk = 0
        return [self.sym24((i,j,k-1)), self.sym24((i,j,k+dk))]
