# Pour compiler :
# python setup.py build_ext --inplace
import sys
sys.path.insert(0,'/home/panda/.local/bin')

import numpy

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

ext_modules = [
    Extension(
        "csystem",
        ["csystem.pyx"],
        extra_compile_args=['-fopenmp'],
        extra_link_args=['-fopenmp'],
    )
]

annotation = False

setup(
    name="csystem-parallel",
    ext_modules = cythonize(ext_modules, annotate=annotation),include_dirs=[numpy.get_include()]
)
