# Pour compiler :
# python setup.py build_ext --inplace
import numpy as np
cimport numpy as np
cimport cython


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def diffusion(double[:,:,::1] a not None, double[:,:,::1] d not None, double rho):
    cdef int i,j,k, Nx, Ny, Nz, m, ip,jp,kp
    cdef int neighbours[21]
    m = 0
    ip = 0
    jp = 0
    kp = 0


    Nx = d.shape[0]-1
    Ny = d.shape[1]-1
    Nz = d.shape[2]-1
    #T diffusion
    cdef double[:,:,::1] dp = rho*np.ones((Nx+1, Ny+1, Nz + 1))
    cdef double[:,:,::1] dpp = rho*np.ones((Nx+1, Ny+1, Nz + 1))

    for j in range(Ny + 1):
        for i in range(j, Nx + 1):
            for k in range(Nz + 1):
                if(a[i,j,k] != 1.):
                    dp[i,j,k] = 0
                    Tneighbours(Nx,Ny,i,j,k, neighbours)
                    for m in range(7):
                        ip = neighbours[m*3]
                        jp = neighbours[m*3+1]
                        kp = neighbours[m*3+2]
                        if(a[ip,jp,kp] != 1.):
                            dp[i,j,k] += d[ip,jp,kp]/7.
                        else:
                            dp[i,j,k] += d[i,j,k]/7.
    #Z diffusion
    for j in range(Ny + 1):
        for i in range(j,Nx + 1):
            for k in range(Nz):
                if(a[i,j,k] != 1.):
                    dpp[i,j,k] = dp[i,j,k]*4./7.
                    Zneighbours(Nz,i,j,k,neighbours)
                    for m in range(2):
                        ip = neighbours[m*3]
                        jp = neighbours[m*3+1]
                        kp = neighbours[m*3+2]
                        if(a[ip,jp,kp] != 1.):
                            dpp[i,j,k] += 3/14.*dp[ip,jp,kp]
                        else:
                            dpp[i,j,k] += 3/14.*dp[i,j,k]
    d[:,:,:] = dpp

cdef inline void sym24(int i, int j, int k, int* ijk) nogil :
    ijk[0] = i
    ijk[1] = j
    ijk[2] = k
    cdef int sum = i + j
    cdef int tmp
    if(i < 0):
        if(j <= 0):
            ijk[0] = -i
            ijk[1] = -j
        elif(sum <= 0):
            ijk[0] = j
            ijk[1]= -sum
    if(i <= 0 and j > 0 and sum > 0):
        ijk[0] = sum
        ijk[1] = -i
    elif(i > 0 and j < 0 and sum >= 0):
            ijk[0] = -j
            ijk[1] = sum
    elif(i >= 0 and j < 0 and sum < 0):
        ijk[0] = -sum
        ijk[1] = i
    if(k < 0):
        ijk[2] = -k
    if(ijk[0] < ijk[1]):
        tmp = ijk[0]
        ijk[0] = ijk[1]
        ijk[1] = tmp

cdef inline void Tneighbours(int Nx,int Ny, int i, int j, int k, int* n) nogil:
    cdef int dj = 1
    cdef int di = 1
    cdef int ijk[3]
    if(j == Ny):
        dj = -2*Ny
    if(i == Nx):
        di = -2*Nx

    sym24(i,j,k, ijk)
    n[0] = ijk[0]
    n[1] = ijk[1]
    n[2] = ijk[2]
    sym24(i-1,j,k, ijk)
    n[3] = ijk[0]
    n[4] = ijk[1]
    n[5] = ijk[2]
    sym24(i,j-1,k, ijk)
    n[6] = ijk[0]
    n[7] = ijk[1]
    n[8] = ijk[2]
    sym24(i-1,j+dj,k, ijk)
    n[9] = ijk[0]
    n[10] = ijk[1]
    n[11] = ijk[2]
    sym24(i,j+dj,k, ijk)
    n[12] = ijk[0]
    n[13] = ijk[1]
    n[14] = ijk[2]
    sym24(i+di,j,k, ijk)
    n[15] = ijk[0]
    n[16] = ijk[1]
    n[17] = ijk[2]
    sym24(i+di,j-1,k, ijk)
    n[18] = ijk[0]
    n[19] = ijk[1]
    n[20] = ijk[2]

cdef inline void Zneighbours(int Nz, int i, int j, int k, int* n) nogil:
    cdef int dk = 1
    cdef int ijk[3]
    if(k == Nz):
        dk = 0
    sym24(i,j,k-1, ijk)
    n[0] = ijk[0]
    n[1] = ijk[1]
    n[2] = ijk[2]
    sym24(i,j,k+dk, ijk)
    n[3] = ijk[0]
    n[4] = ijk[1]
    n[5] = ijk[2]




@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def getCrystal(double[:,:,::1] a not None, double[:,:,::1] c not None, int Nx, int Ny, int Nz):
    cdef int i,j,k, ip, jp, kp
    cdef int ijk[3]
    for i in range(-Nx, Nx+1):
        for j in range(-Ny, Ny+1):
            for k in range(-Nz, Nz+1):
                sym24(i,j,k, ijk)
                ip = ijk[0]
                jp = ijk[1]
                kp = ijk[2]
                c[i+Nx,-j+Ny,k+Nz] = a[ip,jp,kp]
