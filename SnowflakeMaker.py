import os
import errno
import pickle
from PIL import Image

from System import System
from Modeller import Modeller
from Drawer import Drawer

# Permet de faire un (1) flocon
class SnowflakeMaker:
    # Charge les paramètres de flocon
    # Est composé de un System qui est le simulateur de flocon brut ; un Modeller qui va créé un modèle 3D à partir de ça et un Drawer qui va pouvoir dessiner ce modèle en une image.
    def __init__(self, parameters):
        self.filename = parameters["filename"]
        filename = "results/" + self.filename
        os.makedirs(filename, exist_ok=True)
        self.filename = filename + "/" + self.filename

        self.step = 0
        self.last_file_saved = ""
        self.last_model_saved = ""
        self.last_all_saved = ""
        self.all_save = parameters["all_save"]
        self.remove_all_saved = parameters["remove_all_saved"]
        self.all_save_frequency = parameters["all_save_frequency"]
        self.max_step = parameters["max_step"]

        self.take_snapshot = parameters["take_snapshot"]
        self.snapshot_frequency = parameters["snapshot_frequency"]
        self.save_data = parameters["save_data"]
        self.make_model = parameters["make_model"]
        self.save_model = parameters["save_model"]
        self.draw = parameters["draw"]

        self.modeller_output_format = parameters["modeller_output_format"]
        self.drawer_output_format = parameters["drawer_output_format"]
        self.make_gif = parameters["make_gif"]
        self.gif_ms = parameters["gif_ms"]
        self.make_video = parameters["make_video"]
        self.framerate = parameters["framerate"]

        self.remove_old_data = parameters["remove_old_data"]
        self.remove_old_images = parameters["remove_old_images"]
        self.remove_old_models = parameters["remove_old_models"]



        Nxy = parameters["Nxy"]
        Nz = parameters["Nz"]
        rho = parameters["rho"]
        beta = parameters["beta"]
        kappa = parameters["kappa"]
        mu = parameters["mu"]




        self.system = System(Nxy, Nz, rho, beta, kappa, mu)
        total_mass = self.get_system_mass()
        self.density_threshold = parameters["density_threshold"]*total_mass
        self.xyradius_threshold = parameters["radius_threshold"]*self.system.Nxy
        self.zradius_threshold = parameters["radius_threshold"]*self.system.Nz

        r = parameters["r"]
        h = parameters["h"]
        smoothstep = parameters["smoothstep"]

        self.modeller = Modeller(r, h, smoothstep)

        resolution = parameters["resolution"]
        background = parameters["background"]


        self.drawer = Drawer(resolution, background, self.xyradius_threshold, self.zradius_threshold)


    #Fait une simulation. A chaque étape : fait grandir le flocon. Si jamais on doit sauvegarder des données, on le fait ; puis on modélise et dessine si nécessaire le flocon à cette étape.
    def run(self):
        print("\n\n==========\nGenerating snowflake " + self.filename + "...")

        # Boucle de croissance
        while(self.not_finished()):
            self.step += 1

            # Si on doit dessiner ou sauver une étape de la croissance
            if(self.take_snapshot and self.snapshot_frequency != 0):
                if(self.step % self.snapshot_frequency == 0):
                    self.snapshot()

            #Si on veut avoir des sauvegardes de l'état global
            if(self.all_save and self.all_save_frequency != 0):
                if(self.step % self.all_save_frequency == 0):
                    print("\rSaving all data in " + self.filename + ".save...")
                    with open(self.filename + ".save", "wb+") as f:
                        pickle.dump(self, f)
                    self.last_all_saved = self.filename + ".save"

            # Croissance
            self.system.grow()
            print("\rstep", self.step, " / ", self.max_step, " mass =", self.get_system_mass() , " / ", self.density_threshold, "xyradius =", self.system.xyradius, " / ", self.xyradius_threshold, "zradius =", self.system.zradius, " / ", self.zradius_threshold, end ='')

        # Ici le flocon est fini
        # Éventuellement on efface la sauvegarde qui sert si jamais le flocon est interrompu durant la croissance
        if(self.remove_all_saved):
            self.remove(self.last_all_saved)
        else:
            #On peut aussi décider de garder la dernière sauvegarde si on fait des tests sur la partie modélisation/dessin
            if(self.all_save):
                self.all_save = False
                print("\rSaving all data in " + self.filename + ".save...")
                with open(self.filename + ".save", "wb+") as f:
                    pickle.dump(self, f)
                self.last_all_saved = self.filename + ".save"
        #On dessine le flocon à son état final
        self.snapshot(last = True)

    #Éventuellement sauvegarde les données, créé le modèle, et dessin le flocon à son état actuel
    #Si c'est l'étape finale, éventuellement fait un gif ou une vidéo de la croissance.
    #S'occupe de supprimer d'anciens fichiers si nécessaire
    def snapshot(self, last = False):
        filename = self.filename
        if(not last):
            filename += str(int(self.step/self.snapshot_frequency))

        #Sauvegarde éventuelle des données du flocon
        if(self.save_data):
            if(self.remove_old_data):
                if(self.last_file_saved != ""):
                    self.remove(self.last_file_saved)
            self.last_file_saved = filename + ".dat"
            print("\rSaving data" + self.last_file_saved, end = '')
            with open(self.last_file_saved, "wb+") as f:
                pickle.dump(self.system.a, f)

        #Création du modèle 3D
        if(self.make_model):
            print("\nProducing model... " , end = '')
            model = self.modeller.construct(self.system.a, self.modeller_output_format)
            #Sauvegarde possible
            if(self.save_model):
                if(self.remove_old_models):
                    if(self.last_model_saved != ""):
                        self.remove(self.last_model_saved)
                self.last_model_saved = filename + self.modeller_output_format
                print("\rSaving model " + filename + self.modeller_output_format, end = '')
                with open(filename + self.modeller_output_format, "w+") as f:
                    f.write(model)

            # Dessin du flocon
            if(self.draw):
                print("\nProducing image " + filename + self.drawer_output_format, end = '')
                self.drawer.produce(model, filename + self.drawer_output_format)

            # Si flocon finalisé possibilité de gif ou vidéo de croissance si l'on a pris des snapshots réguliers
            if(last):
                if(self.take_snapshot):
                    nb_im = int(self.step / self.snapshot_frequency)

                    if(self.make_gif):
                        output_final = self.filename + ".gif"
                        print("\rProducing final animation " + output_final + "...", end = '')
                        pngs=[Image.open(self.filename + str(i+1) + self.drawer_output_format) for i in range(nb_im)]
                        pngs.append(Image.open(self.filename + self.drawer_output_format))
                        pngs[0].save(output_final, save_all=True, append_images=pngs[1:], duration=self.gif_ms, loop=0)

                    if(self.make_video):
                        print("\rProducing final video " + filename + ".mp4" + "...", end = '')
                        cmd = "ffmpeg -y -f image2 -framerate " + str(self.framerate) + " -i '"
                        cmd += filename + "%d" + self.drawer_output_format + "' -s "
                        cmd += str(self.drawer.width) + "x" + str(self.drawer.height) + " -vcodec libx264 -crf 25  -pix_fmt yuv420p " + filename + ".mp4 2> /dev/null"
                        os.system(cmd)
                    # Éventuellement une fois le gif ou la vidéo faite on peut supprimer les images à chaque étape
                    if(self.remove_old_images):
                        for filename in [self.filename + str(i+1) + self.drawer_output_format for i in range(nb_im)]:
                            self.remove(filename)

    # Conditions d'arrêt de la croissance du flocon : plus assez de vapeur à condenser ; trop grand ; on a trop fait d'étapes c'est interminable !!
    def not_finished(self):
        return self.get_system_mass() > self.density_threshold and self.system.xyradius < self.xyradius_threshold and self.system.zradius < self.zradius_threshold and self.step < self.max_step

    # masse de vapeur restante
    def get_system_mass(self):
        return self.system.d.sum()


    # supprime proprement un fichier
    def remove(self, filename):
        try:
            os.remove(filename)
        except OSError as e: # this would be "except OSError, e:" before Python 2.6
            if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
                raise # re-raise exception if a different error occurred
