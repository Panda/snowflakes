#Heeyyyy ce code sert à faire des flocons, sympa, non ?

# TODO éventuel : système pour changer la graine initiale du flocon + gestion du drift (DÉJÀ VU)
import json
import atexit
import pickle

#Alors là tu vois c'est la classe principale qui sert à faire un (1) flocon
from SnowflakeMaker import SnowflakeMaker

# Comment ça marche ?
# Et bien, tu as le fichier *todofile* qui prend sur chaque ligne un nom de fichier de paramètres ou une commande qui dit de recharger un ancien flocon ou un flocon interrompu.
# Pour chaque ligne, on va lire le fichier de paramètres correspondant et lancer la simulation du flocon avec ces paramètres. Cool !
# snow1.params est là pour donner un exemple de tel fichier.
# Les paramètres de base sont :
# "filename": "essai"   # Le nom qui va identifier le flocon. Les résultats seront dans results/filename/
# "max_step": 5000    # Le nombre d'étape max, on s'arrête si on est au dessus
# "take_snapshot": true # à True si l'on veut sauver la formation du flocon
# "snapshot_frequency": 500 # sauver la formation toutes les x étapes
# "all_save": true # à True si l'on veut pouvoir sauver tout l'état du générateur. Nécessaire pour la fonction de reprise d'un flocon interrompu ou si l'on veut sauver l'état à la fin pour se concentrer après que sur le rendu
# "all_save_frequency": 1000 # sauver tout l'état du générateur toutes les x étapes
# "save_data": false # sauver à chaque snapshot un .dat contenant la présence de flocon ou non
# "make_model": true # faire un modèle 3D à chaque snapshot (WaveFront OBJ file ou povray mesh)
# "save_model": false # sauver le modèle 3D à chaque snapshot
# "draw": true # créer une image avec povray du flocon à chaque snapshot
# "modeller_output_format": ".pov" # .pov pour pouvoir le dessiner ensuite ou .obj pour pourquoi pas l'ouvrir dans blender après (debug surtout)
# "drawer_output_format": ".png" # format d'image de flocon (seul .png a été testé)
# "make_video": true # True si l'on veut faire une vidéo mp4 de la formation du flocon (il faut take_snapshot à True pour ça)
# "framerate": 5 # Combien d'images par seconde dans la vidéo
# "make_gif": false # True si l'on veut faire un gif de la formation du flocon (il faut take_snapshot à True pour ça)
# "gif_ms": 200 # Intervalle de temps entre deux images du gif
# "remove_old_data": true #Supprime le dernier .dat si pas le plus actuel durant la formation
# "remove_old_images": false # Supprime les images à la fin
# "remove_old_models": true # Supprime les modèles à la fin
# "remove_all_saved": false # Supprime les sauvegardes de l'état du flocon
# "Nxy": 30 # Taille du plan dans lequel le flocon va grandir
# "Nz": 10 # Hauteur de l'espace dans lequel le flocon va grandir
#
# #Les paramètres ci-dessous concernent les flocons en eux-même. Voir le papier d'origine pour comprendre en détail.
# "rho": 0.08 # densité de vapeur initiale en tout point non gelé
# "beta": {"01": 6, "10": 2.5, "11": 2, "20": 2.5, "21": 1, "30": 1, "31": 1} #seuils de congélation à dépasser selon les configurations pour avoir une solidification du flocon
# "kappa": {"01": 0.5, "10": 0.1, "11": 0.1, "20": 0.1, "21": 0.1, "30": 0.1, "31": 0.1} # taux de congélation selon les configurations pour avir
# "mu": {"01": 0.0001, "10": 0.0001, "11": 0.0001, "20": 0.0001, "21": 0.0001, "30": 0.0001, "31": 0.0001} # taux de fusion selon les configuration
#
# "radius_threshold": 0.9 # On s'arrête si le flocon atteint presque le bord
# "density_threshold": 0.6666666666666666 # On s'arrête s'il n'y a plus assez de matière (vapeur) à condenser dans l'espace
#
# "r": 0.5773502691896258 #rayon de l'hexagone du prisme fondamental
# "h": 1 # hauteur du prisme fondamental
# "smoothstep": 20 # Nombre de fois où l'on va lisser le modèle pour faire disparaitre les grossiers hexagones
# "resolution": 1024 # Taille de chaque image et de la vidéo et du gif
# "background": "abstract.jpg" # Image de fond pour chaque snapshot dessinée



# On va chercher ici les fichiers de paramètres pour les flocons que l'on désire faire
todofile = "parameters/snowflakes.todo"


#Ici c'est pour gérer un mécanisme sympa qui fait que si t'es sur un long flocon, tu peux interrompre le code et ça reviendra à un état sauvegardé quand tu relances
def exit_handler():
    #On a un flocon qui a été interrompu
    if(sfm is not None and sfm.not_finished()):
        # S'il existe une sauvegarde du flocon seulement, on va modifier le fichier des flocons à faire pour dire de charger un SnowflakeMaker précédemment créé plutôt que d'en créer un nouveau de zéro avec des paramètres
        if(sfm.last_all_saved == ""):
            print("Interruption while snowflake " + param_file + " is not finished.")
        else:
            print("Interruption while snowflake " + param_file + " is not finished. It's progression is saved in " + sfm.last_all_saved)

            files[0] = "LOAD " + sfm.last_all_saved
            with open(todofile, "w+") as todo:
                for p in files:
                    todo.write(p)


# On s'assure qu'on appellera cette fonction à la fin du programme
# Gère les interruptions de flocon
atexit.register(exit_handler)

files = []
sfm = None
# Lecture des flocons à faire
with open(todofile, "r") as todo:
    for param_file in todo:
        files.append(param_file)

#Pour chaque flocon
for param_file in files.copy():
    # Doit-on recharger un flocon interrompu ?
    array = param_file.split(" ")
    if(len(array) > 1 and array[0] == "LOAD"):
        with open(array[1].strip(), "rb") as f:
            sfm = pickle.load(f)
    else:
        #Ou plutôt en créer un à partir du fichier de paramètre
        param_file = param_file.strip()
        with open(param_file, "r") as f:
            params = json.load(f)
            sfm = SnowflakeMaker(params)
    # SnowflakeMaker créé : on lance la simulation
    if(sfm is not None):
        sfm.run()
        #We've finished : on sort la ligne du todofile
        files.pop(0)
        with open(todofile, "w+") as todo:
            for param_file in files:
                todo.write(param_file)
