import pickle
from PIL import Image
import numpy as np
import csystem
import math
import os

#Dessine moi-un flocon
class Drawer:
    def __init__(self, resolution, background, xyradius, zradius):
        self.width = resolution
        self.height = resolution
        self.background = background
        self.xyradius = xyradius
        self.zradius = zradius

    #Prend un modèle povray et le dessine sur fond et avec des lumières.
    def produce(self, model, out):
        with open("scene.ini", "w+") as f:
            ini = "Input_File_Name=scene.pov\nOutput_File_Name=" + out  + "\n+W" + str(self.width) + " +H" + str(self.height)
            f.write(ini)

        povray_scene = """#version 3.7;
#include "colors.inc"

#declare HEIGHT = """ + str(self.xyradius) +""";
#declare DEPTH = """ + str(self.zradius) + """;
camera {
    orthographic
    location <0, 0, HEIGHT>
    angle 90
    up <1,0,0>
    right <0,1,0>
    look_at  <0, 0, 0>
}

global_settings { ambient_light rgb<1,1,1> }
light_source { <""" + str(self.xyradius*math.cos(math.pi/6))+ ", "+ str(self.xyradius*math.sin(math.pi/6)) + """ ,HEIGHT> color rgb <0.3, 1, 1>}
light_source { <""" + str(self.xyradius*math.cos(5*math.pi/6))+ ", "+ str(self.xyradius*math.sin(5*math.pi/6)) + """ ,HEIGHT> color rgb <0.2, 1, 1>}
light_source { <""" + str(self.xyradius*math.cos(-math.pi/2))+ ", "+ str(self.xyradius*math.sin(-math.pi/2)) + """ ,3*HEIGHT/4> color rgb <0.6,1,1>}

plane{
 z, -DEPTH
 pigment {
    image_map {jpeg \"""" + self.background + """\"}
    scale <2*HEIGHT,2*HEIGHT,1>
    translate <HEIGHT,HEIGHT,0>}
}

""" + model + """
pigment { White filter 1 }
finish {
    ambient 0.8
    diffuse 0
    reflection .25
    specular 1
    roughness .01
    phong .75
    phong_size 25
}
interior { ior 1.5 }
}"""

        with open("scene.pov", "w+") as f:
            f.write(povray_scene)

        os.system("povray scene.ini 2> /dev/null")
        os.remove("scene.pov")
        os.remove("scene.ini")
